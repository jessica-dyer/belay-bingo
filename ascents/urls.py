from unicodedata import name
from django.urls import path

from ascents.views import (
    AscentListView,
    AscentDetailView,
    AscentCreateView,
    AscentEditView,
)

urlpatterns = [
    path("ascents/", AscentListView.as_view(), name="ascents"),
    path("<int:pk>/", AscentDetailView.as_view(), name="show_ascent"),
    path("create/", AscentCreateView.as_view(), name="create_ascent"),
    path("<int:pk>/edit/", AscentEditView.as_view(), name="edit_ascent"),
]
