# Generated by Django 4.0.3 on 2022-04-06 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ascents', '0003_alter_ascent_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ascent',
            name='type',
            field=models.CharField(choices=[('Toprope', 'Toprope'), ('Lead', 'Lead'), ('Redpoint', 'Redpoint'), ('Pinkpoint', 'Pinkpoint'), ('Onsight', 'Onsight')], default='Toprope', max_length=10),
        ),
    ]
