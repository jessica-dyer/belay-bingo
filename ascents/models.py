from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class Ascent(models.Model):
    class AscentType(models.TextChoices):
        TOPROPE = "Toprope"
        LEAD = "Lead"
        REDPOINT = "Redpoint"
        PINKPOINT = "Pinkpoint"
        ONSIGHT = "Onsight"

    route = models.ForeignKey(
        "routes.Route", related_name="ascents", on_delete=models.CASCADE
    )
    climber = models.ForeignKey(
        USER_MODEL, related_name="ascents", on_delete=models.SET_NULL, null=True
    )
    type = models.CharField(
        max_length=10, choices=AscentType.choices, default=AscentType.TOPROPE
    )
    date = models.DateField(null=True, blank=True)
    notes = models.TextField()
