from django.contrib import admin
from ascents.models import Ascent

# Register your models here.


class AscentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Ascent, AscentAdmin)
