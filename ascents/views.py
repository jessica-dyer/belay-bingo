from audioop import reverse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from ascents.models import Ascent
from django.contrib.auth.mixins import LoginRequiredMixin
import operator
from django.shortcuts import redirect


# Create your views here.
class AscentListView(LoginRequiredMixin, ListView):
    model = Ascent
    template_name = "ascents/list.html"
    paginate_by = 10

    def get_queryset(self):
        filteredAscents = Ascent.objects.filter(climber=self.request.user)
        sortedAscents = filteredAscents.order_by("-date")
        return sortedAscents


class AscentDetailView(LoginRequiredMixin, DetailView):
    model = Ascent
    template_name = "ascents/detail.html"


class AscentCreateView(LoginRequiredMixin, CreateView):
    model = Ascent
    template_name = "ascents/new.html"
    fields = ["route", "type", "date", "notes"]

    def form_valid(self, form):
        ascent = form.save(commit=False)
        ascent.climber = self.request.user
        ascent.save()
        return redirect("ascents")


class AscentEditView(LoginRequiredMixin, UpdateView):
    model = Ascent
    template_name = "ascents/edit.html"
    fields = ["route", "type", "date", "notes"]
    success_url = reverse_lazy("ascents")
