from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from bingoboard.models import BingoGame
from django.views.generic.edit import CreateView
from django.shortcuts import redirect
import random


class BingoGameListView(ListView):
    model = BingoGame
    template_name = "bingoboards/list.html"

    def get_queryset(self):
        return BingoGame.objects.filter(climber=self.request.user)


class BingoGameBoardView(DetailView):
    model = BingoGame
    template_name = "bingoboards/board.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        goals = []

        for game in BingoGame.objects.filter(pk=self.object.id):
            for goal in game.climbing_goal.all():
                goals.append(goal.route.name)
            for funny_goal in game.funny_goal.all():
                goals.append(funny_goal.goal)
        random.shuffle(goals)
        # cut the data off at length 9
        shortGoals = goals[:8]
        shortGoals.insert(4, "logo")
        context["goals_for_game"] = shortGoals
        return context


class BingoGameBoardCreateView(CreateView):
    model = BingoGame
    template_name = "bingoboards/new.html"
    fields = ["name", "climbing_goal", "funny_goal"]

    def form_valid(self, form):
        game = form.save(commit=False)
        game.climber = self.request.user
        game.save()
        form.save_m2m()
        return redirect("bingo_game", pk=game.id)
