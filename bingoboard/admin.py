from django.contrib import admin
from bingoboard.models import ClimbingGoal, FunnyGoal, BingoGame

# Register your models here.


class ClimbingGoalAdmin(admin.ModelAdmin):
    pass


class FunnyGoalAdmin(admin.ModelAdmin):
    pass


class BingoGameAdmin(admin.ModelAdmin):
    pass


admin.site.register(ClimbingGoal, ClimbingGoalAdmin)
admin.site.register(FunnyGoal, FunnyGoalAdmin)
admin.site.register(BingoGame, BingoGameAdmin)
