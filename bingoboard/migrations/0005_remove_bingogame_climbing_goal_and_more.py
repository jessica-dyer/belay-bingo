# Generated by Django 4.0.3 on 2022-04-06 23:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bingoboard', '0004_rename_goal_climbinggoal_route'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bingogame',
            name='climbing_goal',
        ),
        migrations.AddField(
            model_name='bingogame',
            name='climbing_goal',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bingo_games', to='bingoboard.climbinggoal'),
        ),
        migrations.RemoveField(
            model_name='bingogame',
            name='funny_goal',
        ),
        migrations.AddField(
            model_name='bingogame',
            name='funny_goal',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bingo_games', to='bingoboard.funnygoal'),
        ),
    ]
