from django.apps import AppConfig


class BingoboardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bingoboard'
