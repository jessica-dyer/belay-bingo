from django.urls import path

from bingoboard.views import (
    BingoGameListView,
    BingoGameBoardView,
    BingoGameBoardCreateView,
)

urlpatterns = [
    path("", BingoGameListView.as_view(), name="bingo_list"),
    path("<int:pk>/", BingoGameBoardView.as_view(), name="bingo_game"),
    path("create/", BingoGameBoardCreateView.as_view(), name="bingo_game_create"),
]
