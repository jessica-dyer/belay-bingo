from django.db import models
from django.conf import settings
from abc import ABC, abstractmethod


USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class AbstractGoal(models.Model):
    @abstractmethod
    def get_text(self):
        return "Nothing"


class ClimbingGoal(AbstractGoal):
    route = models.ForeignKey(
        "routes.Route",
        related_name="climbing_goals",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self) -> str:
        return self.route.name

    def get_text(self):
        return self.route.name


class FunnyGoal(AbstractGoal):
    goal = models.CharField(max_length=200)

    def get_text(self):
        return self.goal

    def __str__(self) -> str:
        return self.goal


class BingoGame(models.Model):
    name = models.CharField(max_length=200, null=True)
    created_on = models.DateField(auto_now_add=True, null=True)
    climber = models.ForeignKey(
        USER_MODEL, related_name="bingo_games", on_delete=models.CASCADE, null=True
    )
    climbing_goal = models.ManyToManyField("ClimbingGoal", related_name="bingo_games")
    funny_goal = models.ManyToManyField("FunnyGoal", related_name="bingo_games")

    def __str__(self) -> str:
        return self.name
