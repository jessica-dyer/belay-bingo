from django.urls import path
from routes.views import RouteListView, CreateRouteView, RouteDetailView, EditRouteView

urlpatterns = [
    path("", RouteListView.as_view(), name="list_routes"),
    path("create/", CreateRouteView.as_view(), name="create_route"),
    path("<int:pk>/", RouteDetailView.as_view(), name="show_route"),
    path("<int:pk>/edit/", EditRouteView.as_view(), name="edit_route"),
]
