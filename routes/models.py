from django.db import models
from django.conf import settings

# Create your models here.
class Route(models.Model):
    class RouteType(models.TextChoices):
        TRAD = "Trad"
        SPORT = "Sport"

    class LetterRating(models.TextChoices):
        A = "a"
        B = "b"
        C = "c"
        D = "d"

    name = models.CharField(max_length=250)
    location = models.CharField(max_length=200, null=True)
    type = models.CharField(
        max_length=5, choices=RouteType.choices, default=RouteType.TRAD
    )
    grade = models.CharField(max_length=5, null=True)
    letter_rating = models.CharField(
        max_length=4, choices=LetterRating.choices, null=True, blank=True
    )
    pitches = models.PositiveSmallIntegerField(null=True)
    length = models.PositiveSmallIntegerField(null=True, blank=True)
    description = models.TextField(null=True)
    protection = models.TextField(null=True)
    image = models.ImageField(null=True, upload_to="images/", blank=True)

    def __str__(self) -> str:
        return self.name

    def getRating(self):
        if self.letter_rating != None:
            return self.grade + self.letter_rating
        return self.grade
