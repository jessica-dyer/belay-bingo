from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from routes.models import Route
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import redirect
from django.views.generic.detail import DetailView
from ascents.models import Ascent

# Create your views here.
class RouteListView(ListView):
    model = Route
    template_name = "routes/list.html"


class CreateRouteView(CreateView):
    model = Route
    template_name = "routes/new.html"
    fields = [
        "name",
        "location",
        "type",
        "grade",
        "letter_rating",
        "pitches",
        "length",
        "description",
        "protection",
        "image",
    ]

    def form_valid(self, form):
        route = form.save(commit=False)
        route.save()
        return redirect("list_routes")


class RouteDetailView(DetailView):
    model = Route
    template_name = "routes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        ascents = []
        if self.request.user.is_authenticated:
            for ascent in self.request.user.ascents.all():
                ascents.append(ascent)
        context["ascents_for_route"] = ascents
        return context


class EditRouteView(UpdateView):
    model = Route
    template_name = "routes/edit.html"
    fields = [
        "name",
        "location",
        "type",
        "grade",
        "letter_rating",
        "pitches",
        "length",
        "description",
        "protection",
        "image",
    ]

    def form_valid(self, form):
        route = form.save(commit=False)
        route.save()
        return redirect("show_route", pk=route.id)
