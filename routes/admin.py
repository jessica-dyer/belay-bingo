from django.contrib import admin
from routes.models import Route

# Register your models here.


class RouteAdmin(admin.ModelAdmin):
    pass


admin.site.register(Route, RouteAdmin)
